# logger Actor

## Deploiement local (pour le hot reload)

/Users/arnaud/Dev/15m2/projects/rollmops/core/actors/logger/build/logger_s.wasm


## Test "standalone" via WASH 

```sh
 wash call MDTBVRGVUIQSTFFXS75QLUNG3SWUBC4XDUQVVJCXDHUXM6BDZZKS2LQG Logger.Ping '{}'
```

### Sortie console du host 

Le host est execute via  

```sh
bin/wasmcloud_host foreground --verbose
```

11:39:53.883 [info] Starting actor MDTBVRGVUIQSTFFXS75QLUNG3SWUBC4XDUQVVJCXDHUXM6BDZZKS2LQG
11:40:04.932 [info] [MDTBVRGVUIQSTFFXS75QLUNG3SWUBC4XDUQVVJCXDHUXM6BDZZKS2LQG] [Actor]:[Logger]: Pong !!!

Nota: la premiere ligne montre le relaod de l'Acteur après le build

