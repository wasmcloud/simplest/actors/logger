use wasmbus_rpc::actor::prelude::*;
use wasmcloud_interface_httpserver::{HttpRequest, HttpResponse, HttpServer, HttpServerReceiver};
use wasmcloud_interface_logging::info;

use logger_interface::{Logger, LoggerReceiver};

#[derive(Debug, Default, Actor, HealthResponder)]
#[services(Actor, Logger)]
struct LoggerActor {}

const THIS_ACTOR: &str = "[Actor]:[Logger]:";
const ACTOR_NAME: &str = "logger";

/// Implementation of Logger trait methods
#[async_trait]
impl Logger for LoggerActor {
    async fn display<TS: ToString + ?Sized + std::marker::Sync>(
        &self,
        ctx: &Context,
        arg: &TS,
    ) -> RpcResult<String>
    {
        info!("{} Coooooool !!!!",THIS_ACTOR);
        Ok("The right stuff !".to_string())
    }

    async fn ping(&self, ctx: &Context) -> RpcResult<()> {
        info!("{} Pong !!!",THIS_ACTOR);
        Ok(())
    }
}

